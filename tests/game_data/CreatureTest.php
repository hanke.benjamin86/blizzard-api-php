<?php

namespace BlizzardApi\Test;
use BlizzardApi\ApiException;

class CreatureTest extends ApiTest
{
  /**
   * @throws ApiException
   */
  public function testGet() {
    $data = self::$Wow->creature()->get(42722);
      $this->assertEqual('Young Mastiff', $data->name->en_US);
  }

  /**
   * @throws ApiException
   */
  public function testGetClassic() {
      $data = self::$Wow->creature()->get(107, ['classic' => true]);
      $this->assertEqual('Raptor', $data->name->en_US);
  }

  /**
   * @throws ApiException
   */
  public function testFamilies() {
      $data = self::$Wow->creature()->families();
      $this->assertEqual(82, count($data->creature_families));
  }

  /**
   * @throws ApiException
   */
  public function testFamiliesClassic() {
      $data = self::$Wow->creature()->families(['classic' => true]);
    $this->assertEqual(41, count($data->creature_families));
  }

  /**
   * @throws ApiException
   */
  public function testFamily() {
      $data = self::$Wow->creature()->family(1);
      $this->assertEqual('Wolf', $data->name->en_US);
  }

  /**
   * @throws ApiException
   */
  public function testFamilyClassic() {
      $data = self::$Wow->creature()->family(1, ['classic' => true]);
      $this->assertEqual('Wolf', $data->name->en_US);
  }

  /**
   * @throws ApiException
   */
  public function testFamilyMedia() {
      $data = self::$Wow->creature()->familyMedia(1);
      $this->assertEqual('https://render.worldofwarcraft.com/us/icons/56/ability_hunter_pet_wolf.jpg', $data->assets[0]->value);
  }

  /**
   * @throws ApiException
   */
  public function testFamilyMediaClassic() {
      $data = self::$Wow->creature()->familyMedia(1, ['classic' => true]);
      $this->assertEqual('https://render.worldofwarcraft.com/classic-us/icons/56/ability_hunter_pet_wolf.jpg', $data->assets[0]->value);
  }

  /**
   * @throws ApiException
   */
  public function testTypes() {
      $data = self::$Wow->creature()->types();
      $this->assertEqual(15, count($data->creature_types));
  }

  /**
   * @throws ApiException
   */
  public function testTypesClassic() {
      $data = self::$Wow->creature()->types(['classic' => true]);
      $this->assertEqual(13, count($data->creature_types));
  }

  /**
   * @throws ApiException
   */
  public function testType() {
      $data = self::$Wow->creature()->type(1);
      $this->assertEqual('Beast', $data->name->en_US);
  }

  /**
   * @throws ApiException
   */
  public function testTypeClassic() {
      $data = self::$Wow->creature()->type(1, ['classic' => true]);
      $this->assertEqual('Beast', $data->name->en_US);
  }

  /**
   * @throws ApiException
   */
  public function testDisplayMedia() {
      $data = self::$Wow->creature()->displayMedia(30221);
      $this->assertEqual('https://render.worldofwarcraft.com/us/npcs/zoom/creature-display-30221.jpg', $data->assets[0]->value);
  }

  /**
   * @throws ApiException
   */
  public function testDisplayMediaClassic() {
      $data = self::$Wow->creature()->displayMedia(180, ['classic' => true]);
      $this->assertEqual('https://render.worldofwarcraft.com/classic-us/npcs/zoom/creature-display-180.jpg', $data->assets[0]->value);
  }

  /**
   * @throws ApiException
   */
  public function testSearch() {
      $data = self::$Wow->creature()->search(['search' => 'data.id=40624', '&locale=en_US', '&orderby=id', '&_page=1']);
      $this->assertEqual('Celestial Dragon', $data->results[0]->data->name->en_US);
  }

}